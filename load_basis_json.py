#!/usr/bin/env python3

import sys
import os
# import ase
import json

if len(sys.argv) < 3:
    print("USAGE: ./load_basis_json.py <species_defaults directory> <year tag>")
    sys.exit()
#

DIR, tag = sys.argv[1:]


# DIR = './species_defaults' if len(sys.argv) < 2 else sys.argv[1]

basis_type = ['light', 'tight', 'really_tight']
basis_file = "./basis_aims_common_" + tag +  ".json"

print(DIR, tag, basis_file)
# s_to_z = ase.atom.atomic_numbers
# z_to_s = {val:key for key, val in s_to_z.items()}

# get the list of files
list_files = [os.listdir(os.path.join(DIR, item)) for item in basis_type]
# tmp = [i for i in os.walk(DIR)]

dicbasis = {}
for ii in range(len(basis_type)):
    dic = {}
    for term in list_files[ii]:
        z, s = term.split('_')[:2]
        pth = os.path.join(DIR, basis_type[ii])
        pth = os.path.join(pth, term)
        with open(pth, 'r') as f:
            txt = f.read()
        #
        dic[s] = txt
    #
    dicbasis[basis_type[ii]] = dic
#
# 
with open(basis_file, 'w') as f:
    json.dump(dicbasis, f)
#
