#!/usr/bin/env python3

import os
# import ase
import json

basis_type = ['light', 'tight', 'really_tight']

"""
basis_file = 'basis_aims_common.json'
curdir = os.path.dirname(__file__)
basis_file = os.path.join(curdir, basis_file)
#s_to_z = ase.atom.atomic_numbers
#z_to_s = {val:key for key, val in s_to_z.items()}

with open(basis_file, 'r') as f:
    dicbasis = json.load(f)
#
#
"""


dicbasis = {}
for tag in ['16', '20']:
    basis_file = 'basis_aims_common_' + tag + '.json'
    curdir = os.path.dirname(__file__)
    basis_file = os.path.join(curdir, basis_file)
    #s_to_z = ase.atom.atomic_numbers
    #z_to_s = {val:key for key, val in s_to_z.items()}
    
    with open(basis_file, 'r') as f:
        dicbasis[tag] = json.load(f)
    #
#
del f, os, json, curdir
