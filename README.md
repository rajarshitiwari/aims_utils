# aims_utils (Utility functions and scripts)

These are related to running and processing FHI-AIMS input and out files.
At the moment there are two primary files.

# Loading basis
The file [load_basis\_json.py](load_basis\_json.py) reads the species directory,
and processes it into a dictionary of basis _strings_ and saves them into a json file.

# Functions for Aims input and output
The file [aims\_functions.py](aims\_functions.py) containes a set of useful
functions for processing input/output for FHI-AIMS.

# Background

FHI-AIMS requires two input files geometry.in and control.in. The first file contains the structual info of the molecule/crystal and the second file containes the relevant keywords for calculations, and the information about the atomic 'basis' used. Normally, for high-throughput one needs an automated way of generating these files, and while there are aims calculators in ASE (atomic simulation environment) but they do suffer from certain subtle bugs (such as correctly choosing tiers in the basis), and one might for a more compact, simpler to use tools for the high-throughput utility. This repository is attempting that.

- The first thing that is done, is through this utility one does not need the species\_directory for the basis files. They are loaded directly as a dictionary of strings, and the strings contain the basis file content.
